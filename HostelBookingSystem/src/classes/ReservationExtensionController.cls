public class ReservationExtensionController {

        private final Reservation__c reservation { get; set; }
        public List<Reservation__c> resultReservations{get; set;}
        public String searchText { get; set; }
        public Reservation__c searchRes = new Reservation__c();

        // paging properties
        //private integer counter = 0;
        //private integer list_size = 5;
        //public integer total_size;
        //public string selectedPage { get; set { selectedPage = value; } }
        //pagin properties 
        public Reservation__c getSearchRes() {
                return this.searchRes;
        }

        public void setSearchRes(Reservation__c res) {
                this.searchRes = res;
        }

        //public List<Reservation__c> getResultReservations() {
                //if (selectedPage != '0') counter = list_size * integer.valueOf(selectedPage) - list_size;
                //try { //we have to catch query exceptions in case the list is greater than 2000 rows
                        //this.resultReservations = [SELECT Name, Date_Occupied__c, Release_Date__c, Duration__c, Room__r.Name FROM Reservation__c
                                               //limit :list_size
                                               //offset :counter];
                        //return this.resultReservations;

                //} catch(QueryException e) {
                        //ApexPages.addMessages(e);
                        //return null;
                //}
        //}

        //public void setResultReservations(List<Reservation__c> res) {
                //this.resultReservations= res;
        //}
        private String ascendingOrDescending = null;

        public String setOrder { get; set; }


        public ReservationExtensionController(ReservationController resController) {
                this.reservation = (Reservation__c) resController.getRecord();
                this.resultReservations = (List<Reservation__c>) resController.getRecords();
                //total_size = [SELECT count() FROM Reservation__c];
                //selectedPage = '0';
                this.sortByDate_Occupied();


        }

        private void getSortedResults() {
                String sortCriteria = this.setOrder + ' ' + this.ascendingOrDescending;
                resultReservations = Database.query('Select Name, Date_Occupied__c,Release_Date__c,Duration__c,Room__r.Name from Reservation__c where  Release_Date__c>=TODAY order by ' + sortCriteria);


        }
        private void changeAscendingOrDescending() {

                if (this.ascendingOrDescending == 'ASC') {
                        this.ascendingOrDescending = 'DESC';

                } else {
                        this.ascendingOrDescending = 'ASC';
                }
        }

        public PageReference doSearch() {

                /*resultReservations = (List<Reservation__c>)[FIND :searchText IN NAME FIELDS RETURNING 
                  Reservation__c(Name, Date_Occupied__c,Release_Date__c,Duration__c,Room__r.Name where CALENDAR_MONTH(Date_Occupied__c)=:Date.today().month())][0];*/

                if (this.searchText.trim().length() <= 1 && (searchRes.Date_Occupied__c == null || searchRes.Release_Date__c == null)) {
                        resultReservations = [SELECT Id,Name, Date_Occupied__c, Release_Date__c, Duration__c, Room__r.Name FROM Reservation__c where  Release_Date__c>=TODAY];
                        

                } else if (this.searchText.trim().length() < 1 && (searchRes.Date_Occupied__c != null && searchRes.Date_Occupied__c != null)) {
                        this.resultReservations = [SELECT Id,Name, Date_Occupied__c, Release_Date__c, Duration__c, Room__r.Name FROM Reservation__c where Release_Date__c>=TODAY and (Date_Occupied__c = :searchRes.Date_Occupied__c AND Release_Date__c = :searchRes.Release_Date__c)];
                } else if (searchRes.Date_Occupied__c == null && searchRes.Release_Date__c == null) {
                        resultReservations = (List<Reservation__c>) [FIND :searchText IN NAME FIELDS RETURNING
                                                                     Reservation__c(Id,Name, Date_Occupied__c, Release_Date__c, Duration__c, Room__r.Name where Release_Date__c>=TODAY and CALENDAR_MONTH(Date_Occupied__c) = :Date.today().month())] [0];
                        
                } else if (this.searchText.trim().length() > 1 && (searchRes.Date_Occupied__c != null && searchRes.Release_Date__c != null)) {

                        resultReservations = (List<Reservation__c>) [FIND :searchText IN NAME FIELDS RETURNING
                                                                     Reservation__c(Id,Name, Date_Occupied__c, Release_Date__c, Duration__c, Room__r.Name where Release_Date__c>=TODAY and (Date_Occupied__c = :searchRes.Date_Occupied__c AND Release_Date__c = :searchRes.Release_Date__c))] [0];
                        
                }

                return null;
        }

        public PageReference sortByName() {
                this.setOrder = 'Name';
                changeAscendingOrDescending();
                getSortedResults();

                return null;
        }

        public PageReference sortByDate_Occupied() {

                this.setOrder = 'Date_Occupied__c';
                changeAscendingOrDescending();
                getSortedResults();

                return null;
        }

        public PageReference sortByRelease_Date() {
                this.setOrder = 'Release_Date__c';
                changeAscendingOrDescending();
                getSortedResults();

                return null;

        }

        public PageReference sortByDuration() {
                this.setOrder = 'Duration__c';
                changeAscendingOrDescending();
                getSortedResults();

                return null;
        }


        //paging methods


        //public List<Reservation__c> getReservations() {

                //if (selectedPage != '0') counter = list_size * integer.valueOf(selectedPage) - list_size;
                //try { //we have to catch query exceptions in case the list is greater than 2000 rows
                        //this.resultReservations = [SELECT Name, Date_Occupied__c, Release_Date__c, Duration__c, Room__r.Name FROM Reservation__c
                                               //limit :list_size
                                               //offset :counter];
                        //return this.resultReservations;

                //} catch(QueryException e) {
                        //ApexPages.addMessages(e);
                        //return null;
                //}
        //}

        //public Component.Apex.pageBlockButtons getMyCommandButtons() {

                ////the reRender attribute is a set NOT a string
                //Set<string> theSet = new Set<string> ();
                //theSet.add('block');
                //theSet.add('myButtons');

                //integer totalPages;
                //if (math.mod(total_size, list_size) > 0) {
                        //totalPages = total_size / list_size + 1;
                //} else {
                        //totalPages = (total_size / list_size);
                //}

                //integer currentPage;
                //if (selectedPage == '0') {
                        //currentPage = counter / list_size + 1;
                //} else {
                        //currentPage = integer.valueOf(selectedPage);
                //}

                //Component.Apex.pageBlockButtons pbButtons = new Component.Apex.pageBlockButtons();
                //pbButtons.location = 'top';
                //pbButtons.id = 'myPBButtons';

                //Component.Apex.outputPanel opPanel = new Component.Apex.outputPanel();
                //opPanel.id = 'myButtons';

                ////the Previous button will alway be displayed
                //Component.Apex.commandButton b1 = new Component.Apex.commandButton();
                //b1.expressions.action = '{!Previous}';
                //b1.title = 'Previous';
                //b1.value = 'Previous';
                //b1.expressions.disabled = '{!disablePrevious}';
                //b1.reRender = theSet;

                //opPanel.childComponents.add(b1);

                //for (integer i = 0; i<totalPages; i++) {
                        //Component.Apex.commandButton btn = new Component.Apex.commandButton();

                        //if (i + 1 == 1) {
                                //btn.title = 'First Page';
                                //btn.value = 'First Page';
                                //btn.rendered = true;
                        //} else if (i + 1 == totalPages) {
                                //btn.title = 'Last Page';
                                //btn.value = 'Last Page';
                                //btn.rendered = true;
                        //} else {
                                //btn.title = 'Page ' + string.valueOf(i + 1) + ' ';
                                //btn.value = ' ' + string.valueOf(i + 1) + ' ';
                                //btn.rendered = false;
                        //}

                        //if ((i + 1 <= 5 && currentPage< 5)
                            //|| (i + 1 >= totalPages - 4 && currentPage> totalPages - 4)
                            //|| (i + 1 >= currentPage - 2 && i + 1 <= currentPage + 2))
                        //{
                                //btn.rendered = true;
                        //}

                        //if (i + 1 == currentPage) {
                                //btn.disabled = true;
                                //btn.style = 'color:blue;';
                        //}

                        //btn.onclick = 'queryByPage(\'' + string.valueOf(i + 1) + '\');return false;';

                        //opPanel.childComponents.add(btn);

                        //if (i + 1 == 1 || i + 1 == totalPages - 1) { //put text after page 1 and before last page
                                //Component.Apex.outputText text = new Component.Apex.outputText();
                                //text.value = '...';
                                //opPanel.childComponents.add(text);
                        //}

                //}

                ////the Next button will alway be displayed
                //Component.Apex.commandButton b2 = new Component.Apex.commandButton();
                //b2.expressions.action = '{!Next}';
                //b2.title = 'Next';
                //b2.value = 'Next';
                //b2.expressions.disabled = '{!disableNext}';
                //b2.reRender = theSet;
                //opPanel.childComponents.add(b2);

                ////add all buttons as children of the outputPanel                
                //pbButtons.childComponents.add(opPanel);

                //return pbButtons;

        //}

        //public PageReference refreshGrid() { //user clicked a page number        
                //system.debug('**** ' + selectedPage);
                //return null;
        //}

        //public PageReference Previous() { //user clicked previous button
                //selectedPage = '0';
                //counter -= list_size;
                //return null;
        //}

        //public PageReference Next() { //user clicked next button
                //selectedPage = '0';
                //counter += list_size;
                //return null;
        //}

        //public PageReference End() { //user clicked end
                //selectedPage = '0';
                //counter = total_size - math.mod(total_size, list_size);
                //return null;
        //}

        //public Boolean getDisablePrevious() { //this will disable the previous and beginning buttons
                //if (counter> 0) return false; else return true;
        //}

        //public Boolean getDisableNext() { //this will disable the next and end buttons
                //if (counter + list_size<total_size) return false; else return true;
        //}

        //public Integer getTotal_size() {
                //return total_size;
        //}

        //public Integer getPageNumber() {
                //return counter / list_size + 1;
        //}

        //public Integer getTotalPages() {
                //if (math.mod(total_size, list_size)> 0) {
                        //return total_size / list_size + 1;
                //} else {
                        //return(total_size / list_size);
                //}
        //}

        //paging methods
}