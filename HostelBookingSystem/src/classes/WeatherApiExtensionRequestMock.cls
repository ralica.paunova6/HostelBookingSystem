@isTest
global class WeatherApiExtensionRequestMock implements HttpCalloutMock {

	global HTTPResponse respond(HTTPRequest req) {
		System.assertEquals('http://api.openweathermap.org/data/2.5/weather?q=Plovdiv&units=metric&APPID=f3c52c85f9ca6e5c9a8d6d86d01f1318', req.getEndpoint());
		System.assertEquals('GET', req.getMethod());

		HttpResponse res = new HttpResponse();
		res.setHeader('Content-Type', 'application/json');
		res.setBody('{"coord":{"lon":24.75,"lat":42.15},"weather":[{"id":800,"main":"Clear","description":"clear sky","icon":"01d"}],"base":"stations","main":{"temp":18,"pressure":1020,"humidity":88,"temp_min":18,"temp_max":18},"visibility":10000,"wind":{"speed":2.1,"deg":320},"clouds":{"all":0},"dt":1504072800,"sys":{"type":1,"id":5441,"message":0.0019,"country":"BG","sunrise":1504064675,"sunset":1504112261},"id":728193,"name":"Plovdiv","cod":200}');
		res.setStatusCode(200);
		return res;
	}
}