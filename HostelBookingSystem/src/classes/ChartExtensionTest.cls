@isTest
public class ChartExtensionTest {
	static testmethod void testGetChartData(){
		Hostel__c hostel = new Hostel__c();
		hostel.Name = 'Hostel Test';
		hostel.Email__c = 'example@mail.com';
		hostel.Phone__c = '(+359)9999999999';
		hostel.Address__c = 'bul.6-ti Septemvri';
		insert hostel;
		
		Room__c room = new Room__c();
		room.Hostel__c = hostel.Id;
		room.Name = 'Room Test';
		room.Capacity__c = 3;
		insert room;

		Room__c room1 = new Room__c();
		room1.Hostel__c = hostel.Id;
		room1.Name = 'Another Room Test';
		room1.Capacity__c = 2;
		insert room1;
		
		List<Reservation__c> reservations = new List<Reservation__c>();
		Reservation__c posRes = new Reservation__c();
		posRes.Name = 'Reservation 1';
		posRes.Date_Occupied__c = Date.valueOf('2017-08-29');
		posRes.Release_Date__c = Date.valueOf('2017-08-31');
		posRes.Room__c = room.Id;


		Reservation__c posRes1 = new Reservation__c();
		posRes1.Name = 'Reservation 2';
		posRes1.Date_Occupied__c = Date.valueOf('2017-09-01');
		posRes1.Release_Date__c = Date.valueOf('2017-09-02');
		posRes1.Room__c = room.Id;
		
		Reservation__c posRes2 = new Reservation__c();
		posRes2.Name = 'Reservation 3';
		posRes2.Date_Occupied__c = Date.valueOf('2017-09-03');
		posRes2.Release_Date__c = Date.valueOf('2017-09-04');
		posRes2.Room__c = room.Id;

		Reservation__c posRes3 = new Reservation__c();
		posRes3.Name = 'Reservation 4';
		posRes3.Date_Occupied__c = Date.valueOf('2017-08-28');
		posRes3.Release_Date__c = Date.valueOf('2017-08-31');
		posRes3.Room__c = room1.Id;


		Reservation__c posRes4 = new Reservation__c();
		posRes4.Name = 'Reservation 5';
		posRes4.Date_Occupied__c = Date.valueOf('2017-08-30');
		posRes4.Release_Date__c = Date.valueOf('2017-09-02');
		posRes4.Room__c = room1.Id;
		
		Reservation__c posRes5 = new Reservation__c();
		posRes5.Name = 'Reservation 6';
		posRes5.Date_Occupied__c = Date.valueOf('2017-09-02');
		posRes5.Release_Date__c = Date.valueOf('2017-09-04');
		posRes5.Room__c = room1.Id;

		Reservation__c posRes6 = new Reservation__c();
		posRes6.Name = 'Reservation 7';
		posRes6.Date_Occupied__c = Date.valueOf('2017-08-31');
		posRes6.Release_Date__c = Date.valueOf('2017-09-04');
		posRes6.Room__c = room1.Id;

		
		reservations.addAll(new List<Reservation__c>{posRes,posRes1,posRes2,posRes3,posRes4,posRes5,posRes6});
		insert reservations;

		ChartExtension chartExt = new ChartExtension(new ReservationController());
		List<ChartExtension.Data> resultData = chartExt.getData();
		Integer counter = 0;
		for (ChartExtension.Data data:resultData){
			if (data.data1!=0){
				counter++;
			}
		}
		System.assertEquals(7,counter);
		
		

	
	}
}