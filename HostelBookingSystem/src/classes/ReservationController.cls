public class ReservationController {

        public Reservation__c reservation { get; set; }
        public List<Reservation__c> reservations { get {
                        if (reservations == null) {
                                reservations = [select Id, name, Release_Date__c, Date_Occupied__c, Duration__c, Room__r.Name FROM Reservation__c];

                        }
                        return reservations;

                }
                private set; }

        public ReservationController() {
                Id resId = ApexPages.currentPage().getParameters().get('id');
                /*reservation = (resId == null) ? new Reservation__c() :[SELECT Id, Name, Release_Date__c, Date_Occupied__c, Room__c, Room__r.Name
                  FROM Reservation__c where Id = :resId];
                  System.debug(reservation);*/
                reservation = (Reservation__c) getRecord();

				this.getAvailableRooms();
        }

        //WORK IN PROGRESS
        public PageReference save() {
                try {
                        if (checkValidData()) {

                                Id id = getId();
                                if (id == null) {
                                        Room__c room = [select Id, IsAvailable__c from Room__c where Id = :reservation.Room__c];
                                        room.IsAvailable__c = false;
                                        System.debug(room);
                                        update room;
                                }
                                upsert reservation;
                        } else {
                                return null;

                        }


                } catch(System.DmlException e) {
                        ApexPages.addMessages(e);
                        return null;
                }

                PageReference redirect = ApexPages.currentPage();
                redirect.getParameters().clear();
                redirect.setRedirect(true);
                return redirect;
        }

        public PageReference remove() {
                Id id = getId();
                if (id != null) {

                        reservation = [SELECT id, name, Release_Date__c, Date_Occupied__c, Room__c, Room__r.Name
                                       FROM Reservation__c where id = :getId()];

                        delete reservation;
                }
                PageReference redirect = ApexPages.currentPage();
                redirect.getParameters().clear();
                redirect.setRedirect(true);
                return redirect;
        }

        public PageReference edit() {
                reservation = (Reservation__c) getRecord();
                ApexPages.currentPage().getParameters().clear();
                PageReference redirect = ApexPages.currentPage();
                redirect.getParameters().put('id', reservation.Id);
                System.debug(ApexPages.currentPage().getParameters().get('id'));
                redirect.setRedirect(true);
                return redirect;

        }


        public static Id getId() {
                return ApexPages.currentPage().getParameters().get('id');
        }

        public SObject getRecord() {
                Id objId = getId();
                this.reservation = (objId == null) ? new Reservation__c() :[SELECT id, name, Release_Date__c, Date_Occupied__c, Room__c, Room__r.Name
                                                                            FROM Reservation__c where id = :objId];
                return this.reservation;
        }

        public List<SObject> getRecords() {
                return reservations;
        }

        public Boolean checkValidData() {
                Boolean check = true;
                if (reservation.Name == null && reservation.Date_Occupied__c == null && reservation.Release_Date__c == null) {
                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Please enter reservation details!'));
                        check = false;
                }
                if (reservation.Name == null || reservation.Name.trim().length() < 1) {
                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Please enter valid name!'));
                        check = false;
                }


                List<Reservation__c> checkReservations = new List<Reservation__c> ();

                if (reservation.Id == null) {
                        checkReservations = [select Id, Room__c from Reservation__c where((Date_Occupied__c >= :reservation.Date_Occupied__c and Date_Occupied__c<:reservation.Release_Date__c) or(Release_Date__c> :reservation.Date_Occupied__c and Release_Date__c<:reservation.Release_Date__c)) and Room__c = :reservation.Room__c];
                        if (reservation.Date_Occupied__c >= reservation.Release_Date__c) {
                                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Occupation date must be less than release date!'));
                                check = false;
                        } else if (checkReservations.size()> 0 && reservation.Date_Occupied__c <= reservation.Release_Date__c) {
                                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Warning, 'The room is not available for the chosen dates!'));
                                check = false;

                        }

                        if (reservation.Date_Occupied__c<System.today() || reservation.Release_Date__c <= System.today()) {
                                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Please enter data greather or equals to today\'s date !'));
                                check = false;
                        }

                } else {
                        Reservation__c currRes =[select Id,Name, Date_Occupied__c , Release_Date__c, Room__c from Reservation__c where Id=:reservation.Id];
                        checkReservations = [select Id, Room__c from Reservation__c where Id != :reservation.Id and Room__c = :reservation.Room__c and((Date_Occupied__c >= :reservation.Date_Occupied__c and Date_Occupied__c<:reservation.Release_Date__c) or(Release_Date__c> :reservation.Date_Occupied__c and Release_Date__c<:reservation.Release_Date__c))];
                        if (reservation.Date_Occupied__c >= reservation.Release_Date__c) {
                                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Occupation date must be less than release date!'));
                                check = false;
                        } else if (checkReservations.size()> 0 && reservation.Date_Occupied__c <= reservation.Release_Date__c) {
                                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Warning, 'The room is not available for the chosen dates!'));
                                check = false;
                        }

                        if (reservation.Date_Occupied__c<System.today() || reservation.Release_Date__c <= System.today()) {
                                if (reservation.Date_Occupied__c!=currRes.Date_Occupied__c||reservation.Release_Date__c!=currRes.Release_Date__c){

                                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Please enter data greather or equals to today\'s date !'));
                                        check = false;
                                }
                        }

                }
                return check;
        }
		public List<Room__c> availableRooms{get;set;}

		public List<Room__c> getAvailableRooms(){
			this.availableRooms = [select Id,Name, IsAvailable__c, Capacity__c from Room__c where id in (select Room__c from Reservation__c where Release_Date__c<=TODAY and Date_Occupied__c!=TODAY)];
			return availableRooms;
		}
}