@isTest
public class ReservationControllerExtensionTest {

	static testmethod void testSort() {

		Hostel__c hostel = new Hostel__c();
		hostel.Name = 'Hostel Test';
		hostel.Email__c = 'example@mail.com';
		hostel.Phone__c = '(+359)9999999999';
		hostel.Address__c = 'bul.6-ti Septemvri';
		insert hostel;

		Room__c room = new Room__c();
		room.Hostel__c = hostel.Id;
		room.Name = 'Room Test';
		room.Capacity__c = 3;
		insert room;

		List<Reservation__c> reservations = new List<Reservation__c> ();
		Reservation__c posRes = new Reservation__c();
		posRes.Name = 'Reservation 3';
		posRes.Date_Occupied__c = Date.today();
		posRes.Release_Date__c = Date.valueOf('2017-09-10');
		posRes.Room__c = room.Id;


		Reservation__c posRes1 = new Reservation__c();
		posRes1.Name = 'Reservation 1';
		posRes1.Date_Occupied__c = Date.valueOf('2017-09-01');
		posRes1.Release_Date__c = Date.valueOf('2017-09-02');
		posRes1.Room__c = room.Id;

		Reservation__c posRes2 = new Reservation__c();
		posRes2.Name = 'Reservation 2';
		posRes2.Date_Occupied__c = Date.valueOf('2017-09-03');
		posRes2.Release_Date__c = Date.valueOf('2017-09-06');
		posRes2.Room__c = room.Id;
		reservations.addAll(new List<Reservation__c> { posRes, posRes1, posRes2 });
		insert reservations;

		Id[] fixedSearchResult = new Id[3];
		fixedSearchResult[0] = posRes.Id;
		fixedSearchResult[1] = posRes1.Id;
		fixedSearchResult[2] = posRes2.Id;
		Test.setFixedSearchResults(fixedSearchResult);

		ReservationExtensionController resController = new ReservationExtensionController(new ReservationController());

		resController.sortByName();
		System.assertEquals('Reservation 3', resController.resultReservations.get(0).Name);
		
		resController.sortByDate_Occupied();
		System.assertEquals('Reservation 3', resController.resultReservations.get(0).Name); 

		resController.sortByRelease_Date();
		System.assertEquals('Reservation 3', resController.resultReservations.get(0).Name); 

		resController.sortByDuration();
		System.assertEquals('Reservation 1', resController.resultReservations.get(0).Name);

		resController.sortByDuration();
		System.assertEquals('Reservation 3', resController.resultReservations.get(0).Name);
}

	static testmethod void testSearch() {
		Hostel__c hostel = new Hostel__c();
		hostel.Name = 'Hostel Test';
		hostel.Email__c = 'example@mail.com';
		hostel.Phone__c = '(+359)9999999999';
		hostel.Address__c = 'bul.6-ti Septemvri';
		insert hostel;

		Room__c room = new Room__c();
		room.Hostel__c = hostel.Id;
		room.Name = 'Room Test';
		room.Capacity__c = 3;
		insert room;

		List<Reservation__c> reservations = new List<Reservation__c> ();
		Reservation__c posRes = new Reservation__c();
		posRes.Name = 'Reservation 1';
		posRes.Date_Occupied__c = Date.today();
		posRes.Release_Date__c = Date.valueOf('2017-08-31');
		posRes.Room__c = room.Id;


		Reservation__c posRes1 = new Reservation__c();
		posRes1.Name = 'Reservation 2';
		posRes1.Date_Occupied__c = Date.valueOf('2017-09-01');
		posRes1.Release_Date__c = Date.valueOf('2017-09-02');
		posRes1.Room__c = room.Id;

		Reservation__c posRes2 = new Reservation__c();
		posRes2.Name = 'Reservation 3';
		posRes2.Date_Occupied__c = Date.valueOf('2017-09-03');
		posRes2.Release_Date__c = Date.valueOf('2017-09-04');
		posRes2.Room__c = room.Id;
		reservations.addAll(new List<Reservation__c> { posRes, posRes1, posRes2 });
		insert reservations;

		Id[] fixedSearchResult = new Id[3];
		fixedSearchResult[0] = posRes.Id;
		fixedSearchResult[1] = posRes1.Id;
		fixedSearchResult[2] = posRes2.Id;
		Test.setFixedSearchResults(fixedSearchResult);

		ReservationExtensionController resController = new ReservationExtensionController(new ReservationController());
		Reservation__c testSearchRes = new Reservation__c();

		resController.searchText = 'Reservation 1';
		resController.doSearch();
		System.assertEquals(1, resController.resultReservations.size());

		resController.searchText = '';
		resController.doSearch();
		System.assertEquals(3, resController.resultReservations.size());

		testSearchRes.Date_Occupied__c = Date.today();
		testSearchRes.Release_Date__c = Date.valueOf('2017-08-31');
		resController.setSearchRes(testSearchRes);
		resController.doSearch();
		System.assertEquals(1, resController.resultReservations.size());

		resController.searchText = 'Reservation 1';
		resController.doSearch();
		System.assertEquals(1, resController.resultReservations.size());
	}
}