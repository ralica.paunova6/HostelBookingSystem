@isTest
public class WeatherApiExtensionTest {
	static testmethod void testRespond(){
		Test.setMock(HttpCalloutMock.class,new WeatherApiExtensionRequestMock());
		WeatherApiExtension wApiExt = new WeatherApiExtension(new ReservationController());
		
		//get response from weatherApiExtension
		
		HttpResponse res = WeatherApiExtension.getResponse();
		Test.startTest();
        String contentType = res.getHeader('Content-Type');
        System.assert(contentType=='application/json');
        String actualBody = res.getBody();
        String expectedBody = '{"coord":{"lon":24.75,"lat":42.15},"weather":[{"id":800,"main":"Clear","description":"clear sky","icon":"01d"}],"base":"stations","main":{"temp":18,"pressure":1020,"humidity":88,"temp_min":18,"temp_max":18},"visibility":10000,"wind":{"speed":2.1,"deg":320},"clouds":{"all":0},"dt":1504072800,"sys":{"type":1,"id":5441,"message":0.0019,"country":"BG","sunrise":1504064675,"sunset":1504112261},"id":728193,"name":"Plovdiv","cod":200}';
        System.assertEquals(expectedBody, actualBody);
        System.assertEquals(200, res.getStatusCode());
        
        Map<String,Object> results = (Map<String,Object>)JSON.deserializeUntyped(expectedBody);
        wApiExt.city = String.valueOf(results.get('name'));
		System.assertEquals('Plovdiv',wApiExt.city);
		Map<String, Object> mainResults = (Map<String,Object>) results.get('main');
		wApiExt.temp = String.valueOf(mainResults.get('temp'));
		System.assertEquals('18', wApiExt.temp);
		wApiExt.temp_min = String.valueOf(mainResults.get('temp_min'));
		System.assertEquals('18',wApiExt.temp_min);
		wApiExt.temp_max = String.valueOf(mainResults.get('temp_max'));
		System.assertEquals('18', wApiExt.temp_min); 
		wApiExt.pressure = String.valueOf(mainResults.get('pressure'));
		System.assertEquals('1020', wApiExt.pressure); 
		wApiExt.humidity = String.valueOf(mainResults.get('humidity'));
		System.assertEquals('88', wApiExt.humidity); 
        Test.stopTest();
		

	
	
	
	
	
	
	}
}