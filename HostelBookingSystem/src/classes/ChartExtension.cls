public class ChartExtension {

    //private final Reservation__c reservation { get; set; }
    //public List<Reservation__c> resultReservations { get; set; }


    public ChartExtension(ReservationController resController) {
        //this.reservation = (Reservation__c) resController.getRecord();
        //this.resultReservations = (List<Reservation__c>) resController.getRecords();
    }
    

    public List<Data> getData() {
        return ChartExtension.getChartData();
    }

    /*@RemoteAction
    public static List<Data> getRemoteData() {
        return ChartExtension.getChartData();
    }*/

    public static List<Data> getChartData() {
        List<AggregateResult> results = [select DAY_IN_WEEK(Date_Occupied__c) day, count(ID) total from Reservation__c where Date_Occupied__c = THIS_WEEK group by DAY_IN_WEEK(Date_Occupied__c)];
        List<Integer> temp = new Integer[7];
        for (AggregateResult result : results) {
            if (result.get('day') == 1) {
                temp[0] = (Integer) result.get('total');
            }
            else if (result.get('day') == 2) {
                temp[1] = (Integer) result.get('total');
            }
            else if (result.get('day') == 3) {
                temp[2] = (Integer) result.get('total');
            }
            else if (result.get('day') == 4) {
                temp[3] = (Integer) result.get('total');
            }
            else if (result.get('day') == 5) {
                temp[4] = (Integer) result.get('total');
            }
            else if (result.get('day') == 6) {
                temp[5] = (Integer) result.get('total');
            }
            else if (result.get('day') == 7) {
                temp[6] = (Integer) result.get('total');
            }
        }
        for (Integer i = 0; i<temp.size(); i++) {
            if (temp[i] == null) {
                temp[i] = 0;
            }
        }
        List<Data> data = new List<Data> ();

        data.add(new Data('Sun', temp[0]));
        data.add(new Data('Mon', temp[1]));
        data.add(new Data('Tue', temp[2]));
        data.add(new Data('Wed', temp[3]));
        data.add(new Data('Thu', temp[4]));
        data.add(new Data('Fri', temp[5]));
        data.add(new Data('Sat', temp[6]));

        return data;
    }

    // Wrapper class
    public class Data {
        public String name { get; set; }
        public Integer data1 { get; set; }

        public Data(String name, Integer data1) {
            this.name = name;
            this.data1 = data1;

        }
    }
}