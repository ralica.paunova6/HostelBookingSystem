@isTest
public class ClearRoomScheduleJobTest {
	
	static testMethod void testClearRoomScheduledJob(){
		String seconds =Datetime.now().second().format();
		String min = Datetime.now().minute().format();
		String hour = Datetime.now().hour().format();
		String CRON_EXP =seconds+' '+min+' '+hour+' * * ?';
		Hostel__c hostel = new Hostel__c();
		hostel.Name = 'Hostel Test';
		hostel.Email__c = 'example@mail.com';
		hostel.Phone__c = '(+359)9999999999';
		hostel.Address__c = 'bul.6-ti Septemvri';
		insert hostel;
		
		Room__c room = new Room__c();
		room.Hostel__c = hostel.Id;
		room.Name = 'Room Test';
		room.Capacity__c = 3;
		insert room;

		List<Reservation__c> outdatedReservations = new List<Reservation__c>();
		Date startDate =Date.today().addDays(-10);
		Date realeseDate = Date.today().addDays(-7);
		for (Integer i = 0; i < 4; i++){
			Reservation__c reservation = new Reservation__c();
			reservation.Name ='Test'+i;
			reservation.Date_Occupied__c=startDate;
			reservation.Release_Date__c =realeseDate;
			reservation.Room__c =room.Id;
			room.IsAvailable__c=false;
			outdatedReservations.add(reservation);
		}
		insert outdatedReservations;

		Map<Id, Reservation__c> resMap = new Map<Id, Reservation__c>(outdatedReservations);
        List<Id> resIds = new List<Id>(resMap.keySet());

		Test.startTest();
        
        String jobId = System.schedule('ClearRoomScheduledJob',
            CRON_EXP, 
            new ClearRoomScheduleJob());         
 
        List<Reservation__c> result = [SELECT Id 
            FROM Reservation__c 
            WHERE Id IN :resIds];
        System.assertEquals(resIds.size(), result.size(), 'Tasks exist before job has run');
        
        Test.stopTest();
       
        result = [SELECT Id 
            FROM Reservation__c 
            WHERE Id IN :resIds];
        System.assertEquals(resIds.size(), 
            result.size(), 
            'Tasks were not created');

		List<Room__c> availableRooms = [select Id from Room__c where IsAvailable__c=true];
		System.assertEquals(1, availableRooms.size());

	}
	 

}