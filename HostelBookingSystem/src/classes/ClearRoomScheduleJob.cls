public class ClearRoomScheduleJob implements Schedulable {
	
	public void execute(SchedulableContext sContext){
		List<Room__c> roomsToSetAvailable = [select Id,Name, IsAvailable__c, (select Id from Reservation__r where Release_Date__c<=TODAY) from Room__c where Id in (select Room__c from Reservation__c where Release_Date__c<=TODAY)];
		//List<Reservation__c> resForRemove = [select Id from Reservation__c where Release_Date__c<TODAY or Release_Date__c=TODAY];
		//List<Id> ids = new List<Id>();
		for (Room__c room : roomsToSetAvailable){
			//ids.add(room.Id);
			room.IsAvailable__c=true;
		}
		update roomsToSetAvailable;
		//delete resForRemove;
	}

}