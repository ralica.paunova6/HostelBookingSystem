@isTest
public class ReservationControllerTest {
		
	static testmethod void testSave() {

		Hostel__c hostel = new Hostel__c();
		hostel.Name = 'Hostel Test';
		hostel.Email__c = 'example@mail.com';
		hostel.Phone__c = '(+359)9999999999';
		hostel.Address__c = 'bul.6-ti Septemvri';
		insert hostel;
		

		Room__c room = new Room__c();
		room.Hostel__c = hostel.Id;
		room.Name = 'Room Test';
		room.Capacity__c = 3;
		insert room;
		
		
		List<Reservation__c> resBeforeInsert = [select id from Reservation__c];

		Reservation__c posRes = new Reservation__c();
		posRes.Name = 'Reservation 1';
		posRes.Date_Occupied__c = System.today();
		posRes.Release_Date__c = System.today().addDays(3);
		posRes.Room__c = room.Id;

		Reservation__c negRes = new Reservation__c();
		negRes.Name = 'Reservation 2';
		negRes.Date_Occupied__c = System.today().addDays(- 5);
		negRes.Release_Date__c = System.today();
		posRes.Room__c = room.Id;

		ReservationController resController = new ReservationController();
		resController.reservation = posRes;
		resController.save();
		List<Reservation__c> resAfterInsert =[select id from Reservation__c];

		resController.reservation = negRes;
		resController.save();
		System.assertNotEquals(resBeforeInsert.size(),resAfterInsert.size());


		
		PageReference redirect = ApexPages.currentPage();
		redirect.getParameters().put('id', posRes.Id);

		resController.reservation.Name = 'Reservation Test';
		resController.reservation.Date_Occupied__c = System.today().addDays(- 8);
		resController.reservation.Date_Occupied__c = System.today().addDays(- 10);
		resController.save();
		System.assertEquals(resController.reservations.size(),resAfterInsert.size());


	}

	static testmethod void testCheckValidData() {
		Hostel__c hostel = new Hostel__c();
		hostel.Name = 'Hostel Test';
		hostel.Email__c = 'example@mail.com';
		hostel.Phone__c = '(+359)9999999999';
		hostel.Address__c = 'bul.6-ti Septemvri';
		insert hostel;
		

		Room__c room = new Room__c();
		room.Hostel__c = hostel.Id;
		room.Name = 'Room Test';
		room.Capacity__c = 3;
		insert room;

		Reservation__c posRes = new Reservation__c();
		posRes.Name = 'Reservation 1';
		posRes.Date_Occupied__c = System.today();
		posRes.Release_Date__c = System.today().addDays(3);
		posRes.Room__c = room.Id;

		Reservation__c negRes = new Reservation__c();
		negRes.Name = 'Reservation 2';
		negRes.Date_Occupied__c = System.today().addDays(- 5);
		negRes.Release_Date__c = System.today();
		posRes.Room__c = room.Id;

		ReservationController resController = new ReservationController();
		resController.reservation = posRes;
		System.assertEquals(resController.checkValidData(), true);
		resController.reservation = negRes;
		System.assertEquals(resController.checkValidData(), false);
		insert posRes;

		resController.reservation = [select Id, Name, Date_Occupied__c, Release_Date__c from Reservation__c where Name = :posRes.Name];
		resController.reservation.Name = null;
		resController.reservation.Date_Occupied__c = null;
		resController.reservation.Date_Occupied__c = null;
		System.assertEquals(resController.checkValidData(), false);

	}
	static testmethod void testGetId() {
		Hostel__c hostel = new Hostel__c();
		hostel.Name = 'Hostel Test';
		hostel.Email__c = 'example@mail.com';
		hostel.Phone__c = '(+359)9999999999';
		hostel.Address__c = 'bul.6-ti Septemvri';
		insert hostel;
		

		Room__c room = new Room__c();
		room.Hostel__c = hostel.Id;
		room.Name = 'Room Test';
		room.Capacity__c = 3;
		insert room;

		Reservation__c posRes = new Reservation__c();
		posRes.Name = 'Reservation 1';
		posRes.Date_Occupied__c = System.today();
		posRes.Release_Date__c = System.today().addDays(3);
		posRes.Room__c = room.Id;
		insert posRes;
		PageReference redirect = ApexPages.currentPage();
		redirect.getParameters().put('id', posRes.Id);
		ReservationController resController = new ReservationController();

		System.assertEquals(posRes.Id, ReservationController.getId());

	}
	static testmethod void testGetRecord() {

		Hostel__c hostel = new Hostel__c();
		hostel.Name = 'Hostel Test';
		hostel.Email__c = 'example@mail.com';
		hostel.Phone__c = '(+359)9999999999';
		hostel.Address__c = 'bul.6-ti Septemvri';
		insert hostel;
		

		Room__c room = new Room__c();
		room.Hostel__c = hostel.Id;
		room.Name = 'Room Test';
		room.Capacity__c = 3;
		insert room;

		Reservation__c posRes = new Reservation__c();
		posRes.Name = 'Reservation 1';
		posRes.Date_Occupied__c = System.today();
		posRes.Release_Date__c = System.today().addDays(3);
		posRes.Room__c = room.Id;
		insert posRes;

		Reservation__c testRes = [select ID, Name, Date_Occupied__c, Release_Date__c, Room__c from Reservation__c where Name = :posRes.Name];
		PageReference redirect = ApexPages.currentPage();
		redirect.getParameters().put('id', posRes.Id);
		ReservationController resController = new ReservationController();
		resController.getRecord();
		System.assertEquals(testRes.Name, resController.reservation.Name);

	}

	static testmethod void testEdit() {
		Hostel__c hostel = new Hostel__c();
		hostel.Name = 'Hostel Test';
		hostel.Email__c = 'example@mail.com';
		hostel.Phone__c = '(+359)9999999999';
		hostel.Address__c = 'bul.6-ti Septemvri';
		insert hostel;
		

		Room__c room = new Room__c();
		room.Hostel__c = hostel.Id;
		room.Name = 'Room Test';
		room.Capacity__c = 3;
		insert room;

		Reservation__c posRes = new Reservation__c();
		posRes.Name = 'Reservation 1';
		posRes.Date_Occupied__c = System.today();
		posRes.Release_Date__c = System.today().addDays(3);
		posRes.Room__c = room.Id;
		insert posRes;

		ReservationController resController = new ReservationController();
		Reservation__c testRes = [Select Id, Name, Date_Occupied__c, Release_Date__c from Reservation__c where Name = :'Reservation 1'];
		PageReference redirect = ApexPages.currentPage();
		redirect.getParameters().put('id', testRes.Id);
		resController.edit();
		System.assertEquals(testRes.Name, resController.reservation.Name);
	}
	static testmethod void testRemove() {
		Hostel__c hostel = new Hostel__c();
		hostel.Name = 'Hostel Test';
		hostel.Email__c = 'example@mail.com';
		hostel.Phone__c = '(+359)9999999999';
		hostel.Address__c = 'bul.6-ti Septemvri';
		insert hostel;
		

		Room__c room = new Room__c();
		room.Hostel__c = hostel.Id;
		room.Name = 'Room Test';
		room.Capacity__c = 3;
		insert room;

		Reservation__c posRes = new Reservation__c();
		posRes.Name = 'Reservation 1';
		posRes.Date_Occupied__c = System.today();
		posRes.Release_Date__c = System.today().addDays(3);
		posRes.Room__c = room.Id;
		insert posRes;

		List<Reservation__c> resBeforeRemove = [Select Id, Name, Date_Occupied__c, Release_Date__c from Reservation__c];
		ReservationController resController = new ReservationController();
		PageReference redirect = ApexPages.currentPage();
		redirect.getParameters().put('id', posRes.Id);
		resController.remove();
		List<Reservation__c> resAfterRemove = [Select Id, Name, Date_Occupied__c, Release_Date__c from Reservation__c];
		System.assertNotEquals(resBeforeRemove.size(), resAfterRemove.size());
	}

	static testmethod void testAvailableRooms(){
		Hostel__c hostel = new Hostel__c();
		hostel.Name = 'Hostel Test';
		hostel.Email__c = 'example@mail.com';
		hostel.Phone__c = '(+359)9999999999';
		hostel.Address__c = 'bul.6-ti Septemvri';
		insert hostel;
		

		Room__c room = new Room__c();
		room.Hostel__c = hostel.Id;
		room.Name = 'Room Test';
		room.Capacity__c = 3;
		insert room;

		Reservation__c posRes = new Reservation__c();
		posRes.Name = 'Reservation 1';
		posRes.Date_Occupied__c = System.today().addDays(-3);
		posRes.Release_Date__c = System.today();
		posRes.Room__c = room.Id;

		insert posRes;
		ReservationController resController = new ReservationController();
		List<Room__c> available = resController.getAvailableRooms();
		System.assertEquals(1, available.size()); 

	
	}
}