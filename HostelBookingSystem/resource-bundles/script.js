	function getParamValuesByName (querystring) {
          var qstring = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
          for (var i = 0; i < qstring.length; i++) {
            var urlparam = qstring[i].split('=');
            if (urlparam[0] == querystring) {
               return urlparam[1];
            }
          }
        }

        var id = getParamValuesByName('id');
        if(id!=null){
        console.log('id is not null');
            displayAddEdit();
        }
        
        function displayAddEdit() {
        	document.getElementById('j_id0:j_id3:form-btn').style.backgroundColor = '#37414b';
			document.getElementById('j_id0:j_id3:list-btn').style.backgroundColor = 'transparent';
			document.getElementById('j_id0:j_id3:dashboard-btn').style.backgroundColor = 'transparent';
	        var form = document.getElementById('create-edit-form').style;
	        var list = document.getElementById('list-results').style;
	        var dashboard = document.getElementById('jobTable').style;
	        if (form.display=='none') {
		        form.display ='block';
		        list.display ='none';
		        dashboard.display ='none';
	        }
        }

        function displayList() {
        	document.getElementById('j_id0:j_id3:list-btn').style.backgroundColor = '#37414b';
			document.getElementById('j_id0:j_id3:form-btn').style.backgroundColor = 'transparent';
			document.getElementById('j_id0:j_id3:dashboard-btn').style.backgroundColor = 'transparent';
	        var form = document.getElementById('create-edit-form').style;
	        var list = document.getElementById('list-results').style;
	        var dashboard = document.getElementById('jobTable').style;
	        if (list.display=='none') {
		        form.display ='none';
		        list.display ='block';
		        dashboard.display ='none';
	        }
        }

        function displayDashboard() {
        	document.getElementById('j_id0:j_id3:dashboard-btn').style.backgroundColor = '#37414b';
			document.getElementById('j_id0:j_id3:form-btn').style.backgroundColor = 'transparent';
			document.getElementById('j_id0:j_id3:list-btn').style.backgroundColor = 'transparent';
	        var form = document.getElementById('create-edit-form').style;
	        var list = document.getElementById('list-results').style;
	        var dashboard = document.getElementById('jobTable').style;
	        if (dashboard.display=='none') {
		        form.display ='none';
		        list.display ='none';
		        dashboard.display ='block';
	        }
        }

